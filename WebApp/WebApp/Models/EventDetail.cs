﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class EventDetail : EventList
    {
        public EventDetail()
        {
        }
        public HtmlString Title { get; set; }
        public HtmlString SubTitle { get; set; }
        public HtmlString Image { get; set; }
        public HtmlString ProgramOverview { get; set; }
        public HtmlString ProgramOverviewDescription { get; set; }
        public HtmlString Specifications { get; set; }
        public HtmlString GroupSize { get; set; }
        public HtmlString GroupSizeDescription { get; set; }
        public HtmlString TimeNeeded { get; set; }
        public HtmlString TimeNeededDescription { get; set; }
        public HtmlString SpaceRequirmenta { get; set; }
        public HtmlString SpaceRequirmentaDescription { get; set; }
        public HtmlString IdealFor { get; set; }
        public HtmlString IdealForDescription { get; set; }
        public HtmlString Tailoring { get; set; }
        public HtmlString TailoringDescription { get; set; }


    }
}