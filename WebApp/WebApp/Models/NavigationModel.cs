﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Collections;
using Sitecore.Data.Items;

namespace WebApp.Models
{
    public class NavigationModel
    {
        public ChildList Сhildren { get; set; }
        public Item Parent { get; set; }
        public Item Name { get; set; }
    }
}