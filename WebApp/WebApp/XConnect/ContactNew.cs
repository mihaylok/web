﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.DependencyInjection;
using Sitecore.ListManagement.Services.Model;
using Sitecore.ListManagement.Services.Repositories;
using Sitecore.ListManagement.XConnect.Web;
using Sitecore.SecurityModel;
using Sitecore.XConnect;
using Sitecore.XConnect.Client;
using Sitecore.XConnect.Collection.Model;
using System;
using System.Linq;
using System.Web.Mvc;
//using Website.Models;
using Microsoft.Extensions.DependencyInjection;

namespace WebApp.XConnect
{
    public class ContactNew : Controller
    {
        [HttpPost]
        public JsonResult Subscribe(string email)
        {
            //Sitecore.Analytics.Tracker.Current.Session.IdentifyAs("sitecoreextranet", email);
            SubmitContact(email);
            return Json(new { OK = true });
        }

        // Sync example
        public void SubmitContact(string email)
        {
            Guid? contactId = null;
            using (XConnectClient client = Sitecore.XConnect.Client.Configuration.SitecoreXConnectClientConfiguration.GetClient())
            {
                {
                    try
                    {
                        Contact contact = client.Get(
                            new IdentifiedContactReference("sitecoreextranet", email),
                            new ContactExpandOptions(EmailAddressList.DefaultFacetKey));
                        EmailAddressList emailFacet = null;

                        if (contact == null)
                        {

                            contact = new Contact(new ContactIdentifier("sitecoreextranet", email, ContactIdentifierType.Known));
                            client.AddContact(contact);
                            
                        }
                        else                        
                            emailFacet = contact.GetFacet<EmailAddressList>(EmailAddressList.DefaultFacetKey);
                        

                        if (emailFacet != null)
                        {
                            // Change facet properties
                            emailFacet.PreferredEmail = new EmailAddress(email, true);
                            emailFacet.PreferredKey = "Work";

                            // Set the updated facet
                            client.SetFacet(contact, EmailAddressList.DefaultFacetKey, emailFacet);
                        }
                        else
                        {
                            // Facet is new
                            EmailAddressList emails = new EmailAddressList(new EmailAddress(email, true), "Work");

                            client.SetFacet(contact, EmailAddressList.DefaultFacetKey, emails);
                        }



                        // Create new interaction for this contact
                        Guid channelId = Guid.NewGuid(); // Replace with channel ID from Sitecore
                        string userAgent = "Mozilla/5.0 (Nintendo Switch; ShareApplet) AppleWebKit/601.6 (KHTML, like Gecko) NF/4.0.0.5.9 NintendoBrowser/5.1.0.13341";
                        var interaction = new Interaction(contact, InteractionInitiator.Brand, channelId, userAgent);

                        // Get goal ID - this is the ID of the item in Sitecore
                        var fakeGoalId = Guid.Parse("D2F004EB-766D-4D34-87DF-BB5BA8699D9E");

                        // Create new instance of goal
                        var goal = new Goal(fakeGoalId, DateTime.UtcNow);
                        {
                        };

                        // Add goal to interaction
                        interaction.Events.Add(goal);

                        // Add interaction operation to client
                        client.AddInteraction(interaction);




                        // Submit operations as batch
                        client.Submit();

                        contactId = contact.Id;
                    }
                    catch (XdbExecutionException ex)
                    {
                        throw;
                    }
                }
            }

            if (contactId != null)
            {
                //var contactListRepository = ServiceLocator.ServiceProvider.GetService<IFetchRepository<ContactListModel>>();
                //contactListRepository.GetAll().Where(c => c.Name == "")
                var subscriptionService = ServiceLocator.ServiceProvider.GetService<ISubscriptionService>();
                subscriptionService.Subscribe(Guid.Parse("647aecb4-8ace-40cc-eb28-e3fb4851a9a9"), contactId.Value);
            }
        }
    }
}























////using System;
////using System.Collections.Generic;
////using System.Linq;
////using System.Text;
////using System.Web.Security;

////using Sitecore.Configuration;
////using Sitecore.Security.Accounts;

////namespace WebApp.XConnect
////{
////    /// <summary>
///// This class will be responsible for:
///// 1. Adding new users
///// 2. Editing existing users
///// 3. Deleting users
///// 4. Assigning roles
///// </summary>
////    public class UserMaintenance
////    {
////        /// <summary>
////        /// Creates a new user and edits the profile custom fields
////        /// </summary>
////        /// <param name="domain"></param>
////        /// <param name="firstName"></param>
////        /// <param name="lastName"></param>
////        /// <param name="email"></param>
////        /// <param name="comment"></param>
////        /// <param name="telephoneNumber"></param>
////        /// <param name="jobTitle"></param>
////        public void AddUser(string domain, string firstName, string lastName, string email,
////            string comment, string telephoneNumber, string jobTitle)
////        {
////            string userName = string.Concat(firstName, lastName);
////            userName = string.Format(@"{0}\{1}", domain, userName);
////            string newPassword = Membership.GeneratePassword(10, 3);
////            try
////            {
////                if (!User.Exists(userName))
////                {
////                    Membership.CreateUser(userName, newPassword, email);

////                    // Edit the profile information
////                    Sitecore.Security.Accounts.User user = Sitecore.Security.Accounts.User.FromName(userName, true);
////                    Sitecore.Security.UserProfile userProfile = user.Profile;
////                    userProfile.FullName = string.Format("{0} {1}", firstName, lastName);
////                    userProfile.Comment = comment;

////                    // Assigning the user profile template
////                    userProfile.SetPropertyValue("ProfileItemId", "{AE4C4969-5B7E-4B4E-9042-B2D8701CE214}");

////                    // Have modified the user template to also contain telephone number and job title.
////                    userProfile.SetCustomProperty("TelephoneNumber", telephoneNumber);
////                    userProfile.SetCustomProperty("JobTitle", jobTitle);
////                    userProfile.Save();
////                }
////            }
////            catch (Exception ex)
////            {
////                Sitecore.Diagnostics.Log.Error(string.Format("Error in Client.Project.Security.UserMaintenance (AddUser): Message: {0}; Source:{1}", ex.Message, ex.Source), this);
////            }
////        }

////        /// <summary>
////        /// Edits the user profile and custom fields
////        /// </summary>
////        /// <param name="domain"></param>
////        /// <param name="firstName"></param>
////        /// <param name="lastName"></param>
////        /// <param name="email"></param>
////        /// <param name="comment"></param>
////        /// <param name="telephoneNumber"></param>
////        /// <param name="jobTitle"></param>
////        public void EditUser(string domain, string firstName, string lastName, string email,
////            string comment, string telephoneNumber, string jobTitle)
////        {
////            string userName = string.Concat(firstName, lastName);
////            userName = string.Format(@"{0}\{1}", domain, userName);
////            try
////            {
////                Sitecore.Security.Accounts.User user = Sitecore.Security.Accounts.User.FromName(userName, true);
////                Sitecore.Security.UserProfile userProfile = user.Profile;
////                if (!string.IsNullOrEmpty(email))
////                {
////                    userProfile.Email = email;
////                }
////                if (!string.IsNullOrEmpty(comment))
////                {
////                    userProfile.Comment = comment;
////                }

////                // Have modified the user template to also contain telephone number and job title.
////                if (!string.IsNullOrEmpty(telephoneNumber))
////                {
////                    userProfile.SetCustomProperty("TelephoneNumber", telephoneNumber);
////                }
////                if (!string.IsNullOrEmpty(jobTitle))
////                {
////                    userProfile.SetCustomProperty("JobTitle", jobTitle);
////                }
////                userProfile.Save();
////            }
////            catch (Exception ex)
////            {
////                Sitecore.Diagnostics.Log.Error(string.Format("Error in Client.Project.Security.UserMaintenance (EditUser): Message: {0}; Source:{1}", ex.Message, ex.Source), this);
////            }
////        }

////        /// <summary>
////        /// Deletes a user for a particular domain
////        /// </summary>
////        /// <param name="userName"></param>
////        public void DeleteUser(string userName)
////        {
////            try
////            {
////                Sitecore.Security.Accounts.User user = Sitecore.Security.Accounts.User.FromName(userName, true);
////                user.Delete();
////            }
////            catch (Exception ex)
////            {
////                Sitecore.Diagnostics.Log.Error(string.Format("Error in Client.Project.Security.UserMaintenance (DeleteUser): Message: {0}; Source:{1}", ex.Message, ex.Source), this);
////            }
////        }

////        /// <summary>
////        /// Assigns a user to either the User or Superuser role of that particular domain
////        /// </summary>
////        /// <param name="domain"></param>
////        /// <param name="firstName"></param>
////        /// <param name="lastName"></param>
////        /// <param name="isSuperUser"></param>
////        public void AssignUserToRole(string domain, string firstName, string lastName, bool isSuperUser)
////        {
////            try
////            {
////                ConfigStore userRolesConfig = Sitecore.Configuration.ConfigStore.Load("config");
////                List<ConfigRecord> userRoles = userRolesConfig.RootRecord.GetChildRecords();

////                string userName = string.Concat(firstName, lastName);
////                userName = string.Format(@"{0}\{1}", domain, userName);
////                string domainRole = string.Empty;
////                if (isSuperUser)
////                {
////                    domainRole = string.Format("{0}\\{1}",
////                        domain,
////                        userRoles.SingleOrDefault(role => role.Attributes["IsSuperUser"] == "1").Attributes["name"]);
////                }
////                else
////                {
////                    domainRole = string.Format("{0}\\{1}",
////                        domain,
////                        userRoles.SingleOrDefault(role => role.Attributes["IsSuperUser"] == "0" && role.Attributes["Access"] == "Allow").Attributes["name"]);
////                }
////                UserRoles.FromUser(User.FromName(userName, true)).Add(Role.FromName(domainRole));
////            }
////            catch (Exception ex)
////            {
////                Sitecore.Diagnostics.Log.Error(string.Format("Error in Client.Project.Security.UserMaintenance (AssignUserToRole): Message: {0}; Source:{1}", ex.Message, ex.Source), this);
////            }
////        }
////    }
////}








//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using Sitecore.Data.Items;
//using Sitecore.XConnect;
//using Sitecore.XConnect.Client;
//using Sitecore.XConnect.Collection.Model;
////using Sitecore.Modules.EmailCampaign.ClientApi.UpdateSubscriptions;
//using System.Runtime.Remoting.Contexts;
//using Sitecore.Security.Authentication;
//using Sitecore.Analytics;

//namespace WebApp.XConnect
//{
//    public class Conntact
//    {
//        public void Example()
//        {
//            using (XConnectClient client = Sitecore.XConnect.Client.Configuration.SitecoreXConnectClientConfiguration.GetClient())
//            {
//                try
//                {
//                    var firstContact = new Contact();
//                    client.AddContact(firstContact); // Extension found in Sitecore.XConnect.Operations

//                    // Submits the batch, which contains two operations
//                    client.Submit();
//                }
//                catch (XdbExecutionException ex)
//                {
//                    // Manage exception
//                }
//            }
//        }

//        public void AddContactToContactList(string contactListId, string firstName, string surname, string emailAddress)
//        {
//            Item managerRoot = Sitecore.Data.Database.GetDatabase("master").GetItem("/sitecore/content/Email Campaign");

//            if (managerRoot == null)
//            {
//                throw new Exception("No root manager found at '/sitecore/content/Email Campaign'. Have you installed EXM?");
//            }

//            string managerRootId = managerRoot.ID.ToString();

//            if (!Sitecore.Analytics.Tracker.IsActive)
//                Sitecore.Analytics.Tracker.StartTracking();

//            var currentContact = Sitecore.Analytics.Tracker.Current.Contact;

//            var personal = currentContact.GetFacet<Sitecore.Analytics.Model.Entities.IContactPersonalInfo>("Personal");

//            personal.FirstName = firstName;
//            personal.Surname = surname;

//            var emailAddresses = currentContact.GetFacet<Sitecore.Analytics.Model.Entities.IContactEmailAddresses>("Emails");
//            if (!emailAddresses.Entries.Contains("Email"))
//            {
//                emailAddresses.Entries.Create("Email");
//            }

//            var email = emailAddresses.Entries["Email"];
//            email.SmtpAddress = emailAddress;
//            emailAddresses.Preferred = "Email";

//            //var recipientId = new Sitecore.   // Modules.EmailCampaign.Xdb.XdbContactId(currentContact.ContactId);

//            //Sitecore.Modules.EmailCampaign.ClientApi.UpdateSubscriptions
//            //(recipientId, new[] { contactListId }, new string[] { }, managerRootId, false);
//        }



//        public void AddContactToList(ContactData contact, ContactList list)
//        {
//            ListSubscriptions subscriptions = new ListSubscriptions();
//            var listId = Guid.NewGuid(); /* Replace with real list ID */
//            var isActive = true;
//            var added = DateTime.UtcNow;

//            ContactListSubscription subscription = new ContactListSubscription(added, isActive, listId);

//            subscriptions.Subscriptions.Add(subscription);
//            client.SetListSubscriptions(contact, subscriptions);
//        }

//        public void Test(LoginModel model)
//        {
//            var accountName = string.Empty;
//            var domain = Context.Domain;
//            if (domain != null)
//            {
//                accountName = domain.GetFullName(model.UserName);
//                var result = AuthenticationManager.Login(accountName, model.Password);

//                if (Tracker.Current == null && Tracker.Enabled)
//                {
//                    Tracker.StartTracking();
//                }
//                if (result)
//                {
//                    Tracker.Current.Session.IdentifyAs(domain.Name, model.UserName);
//                    using (Sitecore.XConnect.Client.XConnectClient client = Sitecore.XConnect.Client.Configuration
//                        .SitecoreXConnectClientConfiguration.GetClient())
//                    {
//                        var user = AuthenticationManager.GetActiveUser();
//                        Sitecore.XConnect.Contact contact = new Sitecore.XConnect.Contact(new ContactIdentifier(domain.Name, model.UserName, ContactIdentifierType.Known));
//                        client.AddContact(contact);
//                        //client.Contacts.Where(x => x.Identifiers.Where(i=>i.))
//                        // Facet with a reference object, key is specified
//                        PersonalInformation personalInfoFacet = new PersonalInformation()
//                        {
//                            FirstName = user.DisplayName,
//                            LastName = user.DisplayName
//                        };
//                        FacetReference reference = new FacetReference(contact, PersonalInformation.DefaultFacetKey);
//                        client.SetFacet(reference, personalInfoFacet);
//                        // Facet without a reference, using default key
//                        EmailAddressList emails = new EmailAddressList(new EmailAddress(user.Profile.Email, true), "Home");
//                        client.SetFacet(contact, emails);
//                        // Facet without a reference, key is specified
//                        // Submit operations as batch
//                        await client.SubmitAsync();
//                    }
//                }
//            }
//            return Redirect(SiteContext.Current.StartPath);
//        }


//        public ContactData ConvertContactToContactData(Sitecore.Analytics.Tracking.Contact contact)
//        {
//            return new ContactData()
//            {
//                Identifier = contact.Identifiers.Identifier
//            };
//        }

//        public void AddContactToList(ContactData contact, ContactList list)
//        {
//            ContactListManager listManager = Sitecore.Configuration.Factory.CreateObject("contactListManager", false) as ContactListManager;

//            List<ContactData> contactList = new List<ContactData>();
//            contactList.Add(contact);

//            listManager.AssociateContacts(list, contactList);
//        }

//    }
//}