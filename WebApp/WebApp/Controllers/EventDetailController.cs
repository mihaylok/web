﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class EventDetailController : Controller
    {
        // GET: EventDetail
        public ActionResult Index()
        {
            return View(CreateModel());
        }

        public static EventDetail CreateModel()
        {
            //var item = RenderingContext.Current.ContextItem.Children[1].Children[1].Children[1];
            var item = RenderingContext.Current.ContextItem;

            var eventIntro = new EventDetail()
            {

                Title = new HtmlString(FieldRenderer.Render(item, "Title")),
                SubTitle = new HtmlString(FieldRenderer.Render(item, "SubTitle")),
                Image = new HtmlString(FieldRenderer.Render(item, "Image")),
                ProgramOverview = new HtmlString(FieldRenderer.Render(item, "Program Overview")),
                ProgramOverviewDescription = new HtmlString(FieldRenderer.Render(item, "Program Overview description")),
                Specifications = new HtmlString(FieldRenderer.Render(item, "Specifications")),
                GroupSize = new HtmlString(FieldRenderer.Render(item, "Group Size")),
                GroupSizeDescription = new HtmlString(FieldRenderer.Render(item, "Group Size description")),
                TimeNeeded = new HtmlString(FieldRenderer.Render(item, "Time Needed")),
                TimeNeededDescription = new HtmlString(FieldRenderer.Render(item, "Tite Needed description")),
                SpaceRequirmenta = new HtmlString(FieldRenderer.Render(item, "Space Requirement")),
                SpaceRequirmentaDescription = new HtmlString(FieldRenderer.Render(item, "Space Requirement description")),
                IdealFor = new HtmlString(FieldRenderer.Render(item, "Ideal For")),
                IdealForDescription = new HtmlString(FieldRenderer.Render(item, "Ideal For description")),
                Tailoring = new HtmlString(FieldRenderer.Render(item, "Tailoring")),
                TailoringDescription = new HtmlString(FieldRenderer.Render(item, "Tailoring description")),
                //Color = RenderingContext.Current.Rendering.Parameters["Color"]
                //Image = new HtmlString(FieldRenderer.Render(item, "Image")),
            };

            return eventIntro;

        }
    }
}