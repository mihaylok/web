﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using WebApp.Models;
using WebApp.XConnect;

namespace WebApp.Controllers
{
    public class PromotionController : Controller
    {
        // GET: Promotion
        [HttpGet]
        public ActionResult Index()
        {
            return View(CreateModel());
        }

        public static PromotionModel CreateModel()
        {
            //var item = RenderingContext.Current.ContextItem.Children[1].Children[1].Children[1];
            var item = RenderingContext.Current.ContextItem;

            var eventIntro = new PromotionModel()
            {
                Title = new HtmlString(FieldRenderer.Render(item, "Text for title")),
                Btn = new HtmlString(FieldRenderer.Render(item, "btn"))
            };
            return eventIntro;
        }

        [HttpPost]
        public ActionResult Index(PromotionModel model)
        {
            new ContactNew().Subscribe(model.Email);
            return Redirect("/");
        }
    }

}