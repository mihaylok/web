﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeIntroController : Controller
    {
        // GET: HomeIntro
        public ActionResult Index()
        {
            //return View("", CreateModel());
            return View("~/Views/HomeIntro/Index.cshtml", CreateModel());
            //return PartialView();
        }

        public static HomeIntro CreateModel()
        {
            var item = RenderingContext.Current.ContextItem;
            
            var eventIntro = new HomeIntro()
            {
                //TitlePage = new HtmlString(FieldRenderer.Render(item, "TitlePage")),
                RichText = new HtmlString(FieldRenderer.Render(item, "RichText")),

                Color = RenderingContext.Current.Rendering.Parameters["Color"]
                //Image = new HtmlString(FieldRenderer.Render(item, "Image")),
            };

            return eventIntro;

        }
    }
}