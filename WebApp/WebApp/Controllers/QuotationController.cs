﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Data.Items;
using Sitecore.DependencyInjection;
using Sitecore.ListManagement.XConnect.Web;
using Sitecore.Mvc.Presentation;
using Sitecore.Shell.Framework;
using Sitecore.XConnect;
using Sitecore.XConnect.Client;
using WebApp.Models;
using Sitecore.Analytics;
using Sitecore.Analytics.Model;
using Sitecore.Analytics.Model.Entities;
using Sitecore.XConnect;
using Sitecore.XConnect.Client;
using Sitecore.XConnect.Collection.Model;
using Sitecore.Analytics.Tracking;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.Security.Authentication;
using Sitecore;
using WebApp.XConnect;
//using WebApp.XConnect;

namespace WebApp.Controllers
{

    public class QuotationController : Controller
    {
        // GET: Quotation
        [HttpGet]
        public ActionResult Index()
        {
            return View(new QuotationModel());
        }

        public void CreateClient()
        {
            using (Sitecore.XConnect.Client.XConnectClient client = Sitecore.XConnect.Client.Configuration.SitecoreXConnectClientConfiguration.GetClient())
            {
                try
                {
                    var firstContact = new Sitecore.XConnect.Contact();
                    client.AddContact(firstContact); // Extension found in Sitecore.XConnect.Operations
                    // Submits the batch, which contains two operations
                    client.Submit();
                }
                catch (XdbExecutionException ex)
                {
                    // Manage exception
                }
            }
        }

        [HttpPost]
        public ActionResult Index(QuotationModel model)
        {

            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
                TemplateItem template = master.GetItem("/sitecore/templates/MOB/_Quotation");
                Item newItem = RenderingContext.Current.ContextItem.Add("User", template);
                newItem.Editing.BeginEdit();
                newItem.Fields["Email"].Value = model.Email;
                newItem.Fields["Review"].Value = model.Review;
                newItem.Editing.EndEdit();
                new ContactNew().SubmitContact(model.Email);
            }

            return Redirect("/");
            
            //View(quotationModel);


                                 //var quotationModel = new QuotationModel
                                 //{
                                 //    Email = model.Email,
                                 //    Review = model.Review
                                 //};

            // ContactListManager listManager = Sitecore.Configuration.Factory.CreateObject("contactListManager", false) as ContactListManager;

            //Conntact.AddUser();

            //            var t = new IdentifiedContactReference(Sitecore.Analytics.XConnect.DataAccess.Constants.IdentifierSource, Tracker.Current.Contact.ContactId.ToString("N"));

            //            var contactReference = _contactIdentificationRepository.GetContactReference();

            //            using (var client = SitecoreXConnectClientConfiguration.GetClient())
            //            {
            //                // we can have 1 to many facets
            //                // PersonalInformation.DefaultFacetKey
            //                // EmailAddressList.DefaultFacetKey
            //                // Avatar.DefaultFacetKey
            //                // PhoneNumberList.DefaultFacetKey
            //                // AddressList.DefaultFacetKey
            //                // plus custom ones
            //                var facets = new List<string> { PersonalInformation.DefaultFacetKey };

            //                // get the contact
            //                var contact = client.Get(contactReference, new ContactExpandOptions(facets.ToArray()));

            //    .....
            //}







            //var domain = Context.Domain;

            //XConnectClient client = Sitecore.XConnect.Client.Configuration.SitecoreXConnectClientConfiguration.GetClient();
            //CreateClient();
            //var user = AuthenticationManager.GetActiveUser();
            //var contactIdtest = user.GetProviderUserKey().ToString();








            //var subscriptionService = ServiceLocator.ServiceProvider.GetService<ISubscriptionService>();
            //Guid contactListId = new Guid("647aecb4-8ace-40cc-eb28-e3fb4851a9a9");

            //var userID = Tracker.Current.Contact.ContactId.ToString("N");
            //Guid contactId = new Guid(userID);

            //subscriptionService.Subscribe(contactListId, contactId);
















            //var t = new Sitecore.Analytics.Tracking.Contact().ContactId;   Sitecore.Analytics.Tracking.Contact.IsReadOnly;


            //var cintactid = Sitecore.Analytics.Tracking
            //var subscriptionService = ServiceLocator.ServiceProvider.GetService <ISubscriptionService>( );    GetService<ISubscriptionService>();
            //subscriptionService.Subscribe(contactListId, contactId);


            //using (Sitecore.XConnect.Client.XConnectClient client = Sitecore.XConnect.Client.Configuration.SitecoreXConnectClientConfiguration.GetClient())
            //{
            //    try
            //    {
            //        var firstContact = new Sitecore.XConnect.Contact();
            //        var cont = client.AddContact(firstContact); // Extension found in Sitecore.XConnect.Operations

            //        // Submits the batch, which contains two operations
            //        client.Submit();
            //    }
            //    catch (XdbExecutionException ex)
            //    {
            //        // Manage exception
            //    }
            //}

            //var contactListRepository = ServiceLocator.ServiceProvider.GetService(IFetchRepository<ContactListModel>);
            //var newContactList = new ContactListModel()
            //{
            //    Id = Guid.NewGuid().ToString("B"),
            //    Name = "New Contact List",
            //    Description = "Description of the contact list",
            //    Owner = "Administrator"
            //};
        }


        public QuotationModel CreateModel()
        {
            var eventIntro = new QuotationModel();
            {
                // Email = mo ,
                //Review = RenderingContext.Current.ContextItem.Parent
            };
            return eventIntro;
            //RenderingContext.Current.ContextItem.Add()
        }

        public void Example()
        {
            using (Sitecore.XConnect.Client.XConnectClient client = Sitecore.XConnect.Client.Configuration.SitecoreXConnectClientConfiguration.GetClient())
            {
                try
                {
                    var firstContact = new Sitecore.XConnect.Contact();
                    client.AddContact(firstContact); // Extension found in Sitecore.XConnect.Operations

                    // Submits the batch, which contains two operations
                    client.Submit();
                }
                catch (XdbExecutionException ex)
                {
                    // Manage exception
                }
            }
        }

    }
}