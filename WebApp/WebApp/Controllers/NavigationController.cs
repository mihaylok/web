﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Collections;
using Sitecore.Mvc.Presentation;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class NavigationController : Controller
    {
        // GET: Navigation
        public ActionResult Index()
        {
            return View(CreateModel());
        }
        public NavigationModel CreateModel()
        {
            var eventIntro = new NavigationModel()
            {
                Сhildren = RenderingContext.Current.ContextItem.GetChildren(),
                Parent = RenderingContext.Current.ContextItem.Parent
            };
            return eventIntro;
        }
    }

}